package pl.sda.dao;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import pl.sda.configuration.JdbcConnectionManager;
import pl.sda.configuration.PropertyReader;
import org.junit.jupiter.api.BeforeEach;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import pl.sda.TestUtilities;
import pl.sda.entity.Address;
import pl.sda.entity.Person;

class PersonDaoJpaImplTest extends PersonDaoTest {

    private JdbcConnectionManager jdbcConnectionManager;
    private PersonDaoJpaImpl personDaoJpaImpl;

    @BeforeEach
    void setUp() throws Exception {
        personDaoJpaImpl = new PersonDaoJpaImpl();
        jdbcConnectionManager = new JdbcConnectionManager(PropertyReader.loadConfiguration());
        TestUtilities.setUpDB(jdbcConnectionManager, "db.create.sql");
        TestUtilities.insertDataToDB(jdbcConnectionManager, "db.insertData.sql");
    }

    @Test
    public void testFindAll() {
        List<Person> result = personDaoJpaImpl.findAll();
        System.out.println("\n" + result);
    }

    @Test
    public void testAddNewPerson() {
        Address address = new Address();
        address.setCity("Kraków");
        address.setStreet("Mariacka");
        address.setZipCode("00-111");
        address.setHomeNumber("7b");

        Person person = new Person();
        person.setFirstName("Anna");
        person.setLastName("Kowalska");
        person.setGender("F");
        person.setAddress(address);

        personDaoJpaImpl.addNewPerson(person);
    }

    @Test
    public void testRemovePersonById() {
        personDaoJpaImpl.removePersonById(1);
    }


    @Test
    public void testUpdatePerson() throws ParseException {
        Person pers = new Person();
        pers.setPersonId(1);
        pers.setFirstName("Tomek");
        pers.setLastName("Burak");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateFormat.parse("2000-04-10");
        pers.setDateOfBirth(date);
        pers.setGender("M");

        Address addr = new Address();
        addr.setCity("Berlin");
        addr.setStreet("Eberswalder Str.");
        addr.setZipCode("99-666");
        addr.setHomeNumber("111");

        personDaoJpaImpl.updatePerson(pers, addr);
    }

    @Test
    public void testFindById() {
        Optional<Person> person = personDaoJpaImpl.findById(2);
        System.out.println(person);
    }

    @Test
    public void findByLastName(){
        List<Person> personList = personDaoJpaImpl.findByLastName("Drozda");
        System.out.println(personList);
    }


    @Test
    public void findByFirstName(){
        List<Person> personList = personDaoJpaImpl.findByFirstName("Stefek");
        System.out.println(personList);
    }

    @Test
    public void findAllSortByLastName(){
        List<Person> result = personDaoJpaImpl.findAllSortByLastName();
        System.out.println(result);

    }



//    @AfterEach
//    void cleanUpDatabase() throws SQLException {
//        TestUtilities.cleanUpDB(jdbcConnectionManager, "db.cleanup.sql");
//    }
}


