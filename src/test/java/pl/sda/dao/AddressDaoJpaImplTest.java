package pl.sda.dao;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.sda.TestUtilities;
import pl.sda.configuration.JdbcConnectionManager;
import pl.sda.configuration.PropertyReader;
import pl.sda.entity.Address;

import java.sql.SQLException;
import java.util.List;

public class AddressDaoJpaImplTest extends AddressDaoTest{

    private JdbcConnectionManager jdbcConnectionManager;
    private AddressDaoJpaImpl addressDaoJpaImpl;

    @BeforeEach
    void setUp() throws Exception {
        addressDaoJpaImpl = new AddressDaoJpaImpl();
        jdbcConnectionManager = new JdbcConnectionManager(PropertyReader.loadConfiguration());
        TestUtilities.setUpDB(jdbcConnectionManager, "db.create.sql");
        TestUtilities.insertDataToDB(jdbcConnectionManager, "db.insertData.sql");
    }

    @Test
    public void testFindAll(){
        List<Address> result = addressDaoJpaImpl.findAll();
        System.out.println("\n" + result);
    }

//    @AfterEach
//    void cleanUpDatabase() throws SQLException {
//        TestUtilities.cleanUpDB(jdbcConnectionManager, "db.cleanup.sql");
//    }
}


