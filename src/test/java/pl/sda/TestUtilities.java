package pl.sda;

import pl.sda.configuration.JdbcConnectionManager;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.stream.Collectors;

public class TestUtilities {

    public static void setUpDB(JdbcConnectionManager jdbcConnectionManager, String sqlFile) throws SQLException {
        executeOnDB(jdbcConnectionManager, sqlFile);
    }

    public static void cleanUpDB(JdbcConnectionManager jdbcConnectionManager, String sqlFile) throws SQLException {
        executeOnDB(jdbcConnectionManager, sqlFile);
    }

    public static void insertDataToDB(JdbcConnectionManager jdbcConnectionManager, String sqlFile) throws SQLException {
        executeOnDB(jdbcConnectionManager, sqlFile);

    }

    private static void executeOnDB(JdbcConnectionManager jdbcConnectionManager, String sqlFile) throws SQLException {
        InputStream inputStream = TestUtilities.class.getClassLoader().getResourceAsStream(sqlFile);
        String sqlContent = new BufferedReader(new InputStreamReader(inputStream)).lines().collect(Collectors.joining("\n"));

        try (Connection conn = jdbcConnectionManager.getConnection();
             Statement st = conn.createStatement()) {
            st.executeUpdate(sqlContent);
        }
    }
}
