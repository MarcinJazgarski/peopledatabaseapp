package pl.sda.dao;

import pl.sda.entity.Address;
import pl.sda.entity.Person;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

public class PersonDaoJpaImpl implements PersonDao {

    private static final EntityManagerFactory emf;

    static {
        emf = Persistence.createEntityManagerFactory("jpa-peopleDatabaseMapping");// Create EntityManagerFactory using Persistence. This enable you to create EntityManagers.
    }

    @Override
    public List<Person> findAll() {
        EntityManager entityManager = emf.createEntityManager();
        TypedQuery<Person> query = entityManager.createQuery("FROM Person", Person.class);
        List<Person> personList = query.getResultList();
        entityManager.close();
        return personList;
    }

    @Override
    public Optional<Person> findById(int id) {
        EntityManager entityManager = emf.createEntityManager();
        Person person = entityManager.find(Person.class, id);
        entityManager.close();
        return Optional.ofNullable(person);
    }

    @Override
    public Optional<List<Person>> findByCityName() {
        return Optional.empty();
    }

    @Override
    public void addNewPerson(Person person) {
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(person);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removePersonById(int persondId) {
        EntityManager entityManager = emf.createEntityManager();
        Person p = entityManager.find(Person.class, persondId);
        entityManager.getTransaction().begin();
        entityManager.remove(p);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void updatePerson(Person person, Address address) {
        EntityManager entityManager = emf.createEntityManager();
        Person p = entityManager.find(Person.class, person.getPersonId());
        if (p != null) {
            entityManager.getTransaction().begin();
            p.setFirstName(person.getFirstName());
            p.setLastName(person.getLastName());
            p.setDateOfBirth(person.getDateOfBirth());
            p.setGender(person.getGender());
            p.getAddress().setCity(address.getCity());
            p.getAddress().setStreet(address.getStreet());
            p.getAddress().setZipCode(address.getZipCode());
            p.getAddress().setHomeNumber(address.getHomeNumber());
            entityManager.getTransaction().commit();
            entityManager.close();
        }
    }

    @Override
    public List<Person> findByLastName(String lastName) {
        EntityManager entityManager = emf.createEntityManager();
        TypedQuery<Person> query = entityManager.createQuery(
                "select p " +
                        "from Person p " +
                        "where p.lastName like :name", Person.class)
                .setParameter("name", "%" + lastName + "%");
        List<Person> personList = query.getResultList();
        entityManager.close();
        return personList;
    }


    @Override
    public List<Person> findByFirstName(String firstName) {
        EntityManager entityManager = emf.createEntityManager();
        TypedQuery<Person> query = entityManager.createQuery(
                "select p " +
                        "from Person p " +
                        "where p.firstName like :name", Person.class)
                .setParameter("name", "%" + firstName + "%");
        List<Person> personList = query.getResultList();
        entityManager.close();
        return personList;
    }

    @Override
    public List<Person> findAllSortByLastName() {
        EntityManager em = emf.createEntityManager();

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Person> cq = cb.createQuery(Person.class);
        Root<Person> root = cq.from(Person.class);
        cq.select(root);
        cq.orderBy(cb.asc(root.get("lastName")),
                cb.asc(root.get("firstName"))); //2nd level of sorting by first Name

        TypedQuery<Person> q = em.createQuery(cq);
        List<Person> allPersons = q.getResultList();
        em.close();
        return allPersons;
    }
}
