package pl.sda.dao;

import pl.sda.entity.Address;

import java.util.List;

public interface AddressDao {

    public List<Address> findAll();

}
