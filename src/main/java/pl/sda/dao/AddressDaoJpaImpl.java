package pl.sda.dao;

import pl.sda.entity.Address;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

public class AddressDaoJpaImpl implements AddressDao{

    private static final EntityManagerFactory emf;

    static {
        emf = Persistence.createEntityManagerFactory("jpa-peopleDatabaseMapping");// Create EntityManagerFactory using Persistence. This enable you to create EntityManagers.
    }

    @Override
    public List<Address> findAll() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Address> query = em.createQuery("FROM Address", Address.class);
        List<Address> addressList = query.getResultList();
        em.close();
        return addressList;
    }

}
