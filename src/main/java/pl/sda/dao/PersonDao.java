package pl.sda.dao;

import pl.sda.entity.Address;
import pl.sda.entity.Person;

import java.util.List;
import java.util.Optional;

public interface PersonDao {

    public List<Person> findAll();

    public Optional<Person> findById(int id);

    public Optional<List<Person>> findByCityName();

    public void addNewPerson(Person person);

    public void removePersonById(int id);

    public void updatePerson(Person person, Address address);

    public List<Person> findByLastName(String lastName);

    public List<Person> findByFirstName(String firstName);

    public List<Person> findAllSortByLastName();



    //other to add later
    //*- search for person base on name or surname
    //*- list basic people info sorted by given criteria (name/surname/

}
